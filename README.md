
| [Website](http://links.otrenav.com/website) | [Twitter](http://links.otrenav.com/twitter) | [LinkedIn](http://links.otrenav.com/linkedin)  | [GitHub](http://links.otrenav.com/github) | [GitLab](http://links.otrenav.com/gitlab) | [CodeMentor](http://links.otrenav.com/codementor) |

---

# Numerical optimization class

- Omar Trejo
- January, 2015

This code was developed for the Numerical Optimization class that I attended at
ITAM during 2015. The class was imparted by Dr. Zeferino Parada. The main topic
is constrained numerical optimization.

The main resource for the class was:

- [Nocedal & Wright - Numerical Optimization (2006)](http://www.springer.com/us/book/9780387303031)

Any feedback is welcome!

---

> "The best ideas are common property."
>
> —Seneca
